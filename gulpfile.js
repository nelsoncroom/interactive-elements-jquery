var sass = require('gulp-sass');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var gulp = require('gulp');
var iconify = require("gulp-iconify");
var uglify = require('gulp-uglify');
var pump = require('pump');
// var include = require("gulp-include");

gulp.task('compress', function (cb) {
  pump([
        gulp.src('src/script/*.js'),
        uglify(),
        rename({ suffix: '.min' }),
        gulp.dest('dist')
    ],
    cb
  );
});

gulp.task('sass', function(){
  return gulp.src('src/scss/**/*.scss')
    .pipe(sass()) // Converts Sass to CSS with gulp-sass
    		.pipe(cssmin())
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest('dist'));
});

gulp.task('iconify', function() {
    iconify({
        src: 'images/svg/*.svg',
        pngOutput: 'images/svg/png',
		    scssOutput: 'css/iconify-not-in-use',
        cssOutput:  'css'
    });
});

gulp.task('watch', function(){
  gulp.watch('images/svg/*.svg', ['iconify']); 
  gulp.watch('src/scss/**/*.scss', ['sass']);
  gulp.watch('src/script/**/*.js', ['compress']);
  // Other watchers
})



 
