"use strict";

$(document).ready(function() {

	/* interact ol reveal */
	$('.interact-ol-reveal').each(function(i) {
		var thisContainer = {};

		$(this).find('li').hide();
		$(this).find('li').wrapInner('<div class="inner"></div>');
		
		/* build HTML for element */
		$(this).wrap('<div class="int-ol-reveal-container"></div>');
		
		// apply ID to wrapping element
		if ($(this).attr('data-id')) {
			var bespokeID = $(this).attr('data-id');
			$(this).closest('.int-ol-reveal-container').attr('id', bespokeID);
		} else {
			$(this).closest('.int-ol-reveal-container').attr('id', 'int-ol-reveal-'+sPageID +'-'+(i+1));
		}
		
		$(this).wrap('<div class="int-ol-reveal-container></div>');
		thisContainer = $(this).parent();

		$(thisContainer).prepend('<div class="banner"></div>');
		$(thisContainer).append('<button class="btn-reveal">reveal</button>');

		/* click event */
		$(thisContainer).find('.btn-reveal').click(function() {
			var thisInteract = $(this).closest('.int-ol-reveal-container');
			var numLi = $(thisInteract).find('ol li').length;
			var counter = 0;

			$(thisInteract).find('ol li').each(function(i, elem) {
				if (!$(elem).hasClass('shown')) {
					$(elem).addClass('shown').slideDown('fast');
					counter = 1;
					if (i >= (numLi-1)) {
						$(thisInteract).find('.btn-reveal').fadeOut('fast');
						$(thisInteract).find('ol').addClass('fully-open');
					}
				}
				if (counter == 1) {
					return false;
				}
			});
			return false;
		});
	});

	/* reveal */
	$('.interact-reveal').each(function(i) {
		
		// apply default ID if bespoke one isn't present
		if ($(this).attr('id')) {
			
		} else {
			var bespokeID = $(this).attr('data-id')
			if ($(this).attr('data-id')) {
				$(this).attr('id', bespokeID);
			} else {
				$(this).attr('id', 'int-reveal-'+sPageID +'-'+(i+1));
			}
		}

		/* init elments */
		$(this).find('button').show();
		$(this).find('.panel').hide();
		$(this).find('.panel').prepend('<a class="close">close</a>');

		$(this).find('button:not(.close)').click(function() {
			var thisReveal = $(this).closest('.interact-reveal');
			var thisData = $(this).attr('data-target');
			var thisBtn = $(this);

			$('button.active').removeClass('active');
			$(thisBtn).addClass('active');

			if ($('.panel.active').length > 0) {
				$('.panel.active').fadeOut('fast', function() {
					$(thisReveal).find('.panel.active').removeClass('active');
					$(thisReveal).find('.'+thisData).addClass('active').fadeIn('fast');
				});
			} else {
				$(thisReveal).find('.panel.active').removeClass('active');
				$(thisReveal).find('.'+thisData).addClass('active').fadeIn('fast');
			}	
			return false;
		});

		$(this).find('.close').click(function() {
			var thisReveal = $(this).closest('.interact-reveal');
			$(thisReveal).find('.active').removeClass('active');
			$(this).closest('.panel').fadeOut('fast');
			return false;
		});
	});

	/* scenario clickers */
	$('.interact-scenario').each(function(i) {
		var thisWrap = [];
		var thisContainer = {};
		var listLength = $(this).find('li').length;

		/* build HTML for clicker */
		$(this).find('li').hide();
		$(this).addClass('content').wrap('<div class="interact-scenario-container"></div>');
		thisContainer = $(this).parent();

		// apply ID to wrapping element
		if ($(this).attr('data-id')) {
			var bespokeID = $(this).attr('data-id');
			$(thisContainer).attr('id', bespokeID);
		} else {
			$(thisContainer).attr('id', 'scenario-'+sPageID +'-'+(i+1));
		}

		$(this).find('li').each(function(i) {
			$(this).addClass('panel panel-'+(i+1)).prepend('<a class="close">close</a>');
		});
		$(thisContainer).prepend('<ul class="nav"></ul>');

		for (i = 0; i < listLength; i++) {
			$(thisContainer).find('.nav').append('<li data-panel="panel-'+(i+1)+'"><button>'+(i+1)+'</button></li>');
		}
		$(thisContainer).find('.nav').append('<li class="back"><button>back</button></li>');

		/* bind click events */
		$(thisContainer).find('.nav button').click(function() {
			var thisScenario = $(this).closest('.interact-scenario-container'); 
			var thisLi = $(this).parent();

			/* back button */
			if ($(thisLi).hasClass('back')) {
				$(thisScenario).find('ol.content li.active').removeClass('active').fadeOut('fast');
				$(thisScenario).find('.active').removeClass('active');
			} else {
				/* all other buttons */
				$(thisScenario).find('.nav li').removeClass('active');
				var panelClass = $(thisLi).addClass('active').attr('data-panel');

				/* if a panel is already shown */
				if ($(thisScenario).find('ol.content li.active').length > 0) {
					$(thisScenario).find('ol.content li.active').removeClass('active').fadeOut('fast', function() {
						$(thisScenario).find('.'+panelClass).addClass('active').fadeIn();
					});
				} else {
					/* if no panel shown */
					$(thisScenario).find('.'+panelClass).addClass('active').fadeIn();
				}				
			}
			return false;
		});
		/* close button */
		$(thisContainer).find('a.close').click(function() {
			var thisScenario = $(this).closest('.interact-scenario-container'); 
			$(thisScenario).find('ol.content li.active').removeClass('active').fadeOut('fast');
			$(thisScenario).find('.active').removeClass('active');
			return false;
		});
	});
});
